
var User = require('../src/controllers/user')
var Index = require('../src/controllers/index')

module.exports = function (app) {
   app.use(function (req, res, next) {

     app.locals.user = req.session.user
 
      next()
   })
 


 
   // 首页
   app.get('/',Index.index)
 
   // 用户
    app.post('/user/signin', User.signin) // 登录页面
    app.post('/user/signup', User.signup) // 注册页面
    app.get('/logout', User.logout) // 登出

 
 }