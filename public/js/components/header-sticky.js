// Header Sticky
var HeaderSticky = function() {
  'use strict';

  // Handle Header Sticky
  var handleHeaderSticky = function() {
    let border_black = '1px solid #222324'
    let black = '#222324'
    // On loading, check to see if more than 15rem, then add the class
    if ($('.js__header-sticky').offset().top > 15) {
      $('.js__header-sticky').addClass('s-header__shrink');
      $('.sign-group').css('border',border_black)
      $('.sign-btn.sign-hide').css('color',black)
    }

    // On scrolling, check to see if more than 15rem, then add the class
    $(window).on('scroll', function() {
      if ($('.js__header-sticky').offset().top > 15) {
        $('.js__header-sticky').addClass('s-header__shrink');
        $('.sign-group').css('border',border_black)
        $('.sign-btn,.sign-hide').css('color',black)
      } else {
        $('.js__header-sticky').removeClass('s-header__shrink');
        $('.sign-group').css('border','1px solid #fff')
        $('.sign-btn,.sign-hide').css('color','#fff')
      }
    });
  }

  return {
    init: function() {
      handleHeaderSticky(); // initial setup for Header Sticky
    }
  }
}();

$(document).ready(function() {
  HeaderSticky.init();
});