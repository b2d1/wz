let scrollTo = (el, offset) => {
    let $el = $(el)
    $el.click(() => {
        $("html").animate({ scrollTop: offset }, "slow")
    })
}

let overplay = (el) => {
    let $el = $(el)
    $el.click(() => {
        if ($el.hasClass("-is-active")) {
            setTimeout(() => {
                $('.sign-group,.hello').fadeIn()
            }, 650);
            
        }
        else {
            $('.sign-group,.hello').hide()
        }
    })

}

let sign = () => {
    let $to_signup = $('.to-signup')
    let $to_signin = $('.to-signin')
    let $signin_modal = $('.signin-modal')
    let $signup_modal = $('.signup-modal')
    let is_signin = true
    $to_signup.click(() => {
        if (is_signin) {
            is_signin = false
            $signin_modal.hide()
            $signup_modal.show()
        }
    })
    $to_signin.click(() => {
        if (!is_signin) {
            is_signin = true
            $signup_modal.hide()
            $signin_modal.show()
        }
    })

}


// 调用
scrollTo('.s-scroll-to-section-v1--bc', $(window).height())
overplay('.s-header__trigger')
sign()

