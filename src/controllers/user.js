var User = require('../models/user')
var mongoose = require('mongoose')
// 用户注册
exports.showSignup = function (req, res) {
  res.render('user/signup', {
    title: '注册页面'
  })
}

exports.showSignin = function (req, res) {
  res.render('user/signin', {
    title: '登录页面'
  })
}


// 用户注册
exports.signup = function (req, res) {
    
  var _user = req.body.user

  User.findOne({name: _user.name}, function (err, user) {
    if (err) {
      console.log(err)
    }

    if (user) {
      console.log('用户已存在')
    } else {
      var user = new User(_user)

      user.save(function (err, newUser) {
        if (err) {
          console.log(err)
        }

        res.redirect('/')
      })
    }
  })
}

// 用户登录
exports.signin = function (req, res) {
  var _user = req.body.user
  var name = _user.name
  var password = _user.password
  User.findOne({name: name}, function (err, user) {
    if (err) {
      console.log(err)
    }

    if (!user) {
        console.log('用户不存在')
      return res.redirect('/')
    }

    user.comparePassword(password, function (err, isMatch) {
      if (err) {
        console.log(err)
      }

      if (isMatch) {
        req.session.user = user
        console.log('登录成功')
        return res.redirect('/')
      } else {
        console.log('密码错误')
        return res.redirect('/')
      }
    })
  })
}

exports.logout = function (req, res) {
  delete req.session.user
  // delete app.locals.user

  res.redirect('/')
}

exports.del = function (req, res) {
  var id = req.query.id

  if (id) {
    User.remove({ _id: id }, function (err, user) {
          if (err) {
              console.log(err)
          } else {
              res.json({
                  success: 1
              })
          }
      })
  }
}

// 用户列表
exports.list = function (req, res) {
  User.fetch(function (err, users) {
    if (err) {
      console.log(err)
    }

    res.render('userlist', {
      title: '用户列表页',
      users: users
    })
  })
}


// 管理员
exports.signinRequired = function (req, res, next) {
    var user = req.session.user
  
    if (!user) {
      return res.redirect('/signin')
    }
  
    next()
  }
  
  // 管理员
  exports.adminRequired = function (req, res, next) {
    var user = req.session.user
  
    if (user.role <= 10) {
      return res.redirect('/signin')
    }
    next()
  }
  